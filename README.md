# sweetspot for dev

> A Vue.js project made by Ignacio Olalde (igolalde@gmail.com)

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For detailed explanation on how things work, checkout the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).




# sweetspot

## Requirements

* node@5 or later

## FAQ

### Where should I put application code?

Application code is expected to be contained within `app/`.

### How to start the server?

```
/sweetspot$: npm run serve
```

This command will open your default browser @ `localhost:9000`.

### How to load 3rd party packages from node_modules?

The server is prepared to load both directories: `.` as well as `app/`. Once your external dependency is installed, it can be directly imported from `app/index.html` by using the following path: `/node_modules/path/to/file`.
