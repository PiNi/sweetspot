import MutationTypes from './mutation-types.js'

export default {
  [MutationTypes.SELECT_INDEX] (state, index) {
    state.activeIndex = index
    state.asideOpen = false
  },
  [MutationTypes.SET_LOADING] (state, value) {
    state.loading = value
  },
  [MutationTypes.SET_USERS] (state, members) {
    state.members = members
  }
}
