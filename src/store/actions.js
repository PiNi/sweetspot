import MembersApi from '@/api/Members'
import MutationTypes from '@/store/mutation-types.js'

export default {
  select ({state, commit}, index) {
    commit(MutationTypes.SELECT_INDEX, index)
  },
  fetchMembers ({state, commit}) {
    if (state.loading) return
    commit(MutationTypes.SET_LOADING, true)
    MembersApi.fetchMembers().then((members) => {
      commit(MutationTypes.SELECT_INDEX, null)
      commit(MutationTypes.SET_USERS, members)
      commit(MutationTypes.SET_LOADING, false)
    }).catch(() => {
      alert('Debe haber una instancia corriendo en http://localhost:9000 (`npm run serve`)')
      commit(MutationTypes.SET_LOADING, false)
    })
  },
  selectNext ({state, commit}) {
    if (state.activeIndex === state.members.length - 1) {
      commit(MutationTypes.SELECT_INDEX, 0)
    } else {
      commit(MutationTypes.SELECT_INDEX, state.activeIndex + 1)
    }
  },
  selectPrev ({state, commit}) {
    if (state.activeIndex === 0) {
      commit(MutationTypes.SELECT_INDEX, state.members.length - 1)
    } else {
      commit(MutationTypes.SELECT_INDEX, state.activeIndex - 1)
    }
  }
}
