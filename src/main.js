import Vue from 'vue'
import App from './App.vue'

import store from './store'

import './scss/main.scss'

Vue.config.env = process.env.NODE_ENV
Vue.config.baseUrl = Vue.config.env === 'development' ? 'http://localhost:9000' : ''

/* eslint-disable no-new */
new Vue({
  store,
  el: '#app',
  render: h => h(App)
})
