export default {
  activeMember (state) {
    if (state.activeIndex == null) return null
    else return state.members[state.activeIndex]
  },
  membersCount (state) {
    return state.members.length
  }
}
