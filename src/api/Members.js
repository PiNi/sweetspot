import axios from 'axios'
import Vue from 'vue'

export const fetchMembersNoDelay = () => {
  return axios.get(`${Vue.config.baseUrl}/assets/members.json`)
    .then((response) => response.data)
}

export const fetchMembers = () => {
  return new Promise((resolve, reject) => {
    setTimeout(() => fetchMembersNoDelay().then(resolve, reject), 2000)
  })
}

export default {
  fetchMembers
}
